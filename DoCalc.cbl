       IDENTIFICATION DIVISION. 
       PROGRAM-ID. DoCalc.
       AUTHOR. Benjamas.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 FirstNum PIC 9 VALUE ZEROS.
       01 SecondNum PIC 9 VALUE ZEROS.
       01 CalcResult PIC 99 VALUE 0.
       01 UserPrompt PIC X(38) VALUE 
           "Please enter single digit number".
       PROCEDURE DIVISION .
       CalculateResult.
           DISPLAY UserPrompt 
           ACCEPT FirstNum 
           DISPLAY UserPrompt 
           ACCEPT SecondNum 
           COMPUTE CalcResult = FirstNum + SecondNum 
           DISPLAY "Result is = " , CalcResult 
           STOP RUN .